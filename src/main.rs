use png::{Info, Reader, Encoder};
use postgres::{Client, Error, NoTls, SimpleQueryMessage};
use std::path::Path;
use std::fs::File;
use std::io::{BufWriter, Write};
use clap::Parser;

#[derive(Parser)]
struct Cli {
    con: String,
    #[arg(short, long)]
    init: bool,
    #[arg(short, long)]
    encode: Option<String>,
    #[arg(short, long)]
    decode: Option<String>,
    #[arg(short, long)]
    script: Option<String>,
}

fn main() {

    let args = Cli::parse();
    let mut client = connect(&args.con);

    if args.init {
        create_schema(&mut client).unwrap();
        println!("Schema initialized.");
    } 
    if let Some(filename) = &args.decode {
        decode(filename, &mut client);
    }
    if let Some(filename) = &args.script {
        exec_script(filename, &mut client);
    }
    if let Some(filename) = &args.encode {
        encode(filename, &mut client);
    }
    client.close().unwrap();
}

fn decode(file: &str, client: &mut Client) {

    let reader = read_png(file);

    if reader.info().color_type != png::ColorType::Indexed {
        panic!("Not indexed!")
    }

    match decode_to_db(reader, client) {
        Ok(_) => println!("File {} decoded successfylly to database.", file),
        Err(e) => panic!("ERROR: {}", e)
    }
}

fn read_png(file: &str) -> Reader<File> {

    let decoder = match File::open(file) {
        Ok(f) => png::Decoder::new(f),
        Err(e) => panic!("Error when reading file {}: {}", file, e)
    };
    match decoder.read_info() {
        Ok(r) => r,
        Err(e) => panic!("Error when decoding PNG {}: {}", file, e)
    }
}

fn decode_to_db(mut reader: Reader<File>, client: &mut Client) -> Result<u64, Error> {

    create_schema(client)?;
    make_palette(reader.info(), client)?;
    make_pixels(&mut reader, client)
}

fn make_pixels(reader: &mut Reader<File>, client: &mut Client) -> Result<u64, Error> {

    let mut buf = vec![0; reader.output_buffer_size()];
    // Read the next frame. An APNG might contain multiple frames.
    let n = match reader.info().animation_control {
        None => 1,
        Some(a) => a.num_frames
    };

    if n < 1 {
        panic!("Error: No frames in image!");
    }
    let mut writer = client.copy_in("COPY png.pixels FROM stdin")?;

    for i in 1..(n+1) {

        let frame = reader.next_frame(&mut buf).unwrap();
        
        let pixels = &buf[..frame.buffer_size()];
        let mut x : u32 = 0;
        let mut y : u32 = 0;

        for pixel in pixels.iter() {
            writer.write(format!("{}\t{}\t{}\t{}\n", 
                &(i as i32), &(x as i32), &(y as i32), &(*pixel as i32)
            ).as_bytes()).unwrap();
            x += 1;
            if x == reader.info().width {
                x = 0;
                y += 1;
            }
        }
    }
    writer.finish()
}

fn make_palette(info: &Info, client: &mut Client) -> Result<Vec<SimpleQueryMessage>, Error> {

    let palette = info.palette.as_ref().unwrap();
    for i in 1..palette.len()/3 {
        let k = i*3;
        client.execute(
            "INSERT INTO png.palette VALUES ($1, $2, $3, $4)",
            &[&((i-1) as i32),
              &(palette[k-3] as i32),
              &(palette[k-2] as i32),
              &(palette[k-1] as i32)]
        )?;
    }
    Ok(Vec::new())
}

fn create_schema(client: &mut Client) -> Result<Vec<SimpleQueryMessage>, Error> {
    client.simple_query("DROP SCHEMA IF EXISTS png CASCADE;")?;
    client.simple_query("CREATE SCHEMA png;")?;
    client.simple_query("CREATE TABLE png.palette(i int, r int, g int, b int);")?;
    client.simple_query("CREATE TABLE png.pixels(img int, x int, y int, c int);")
    client.simple_query("CREATE INDEX ON png.pixels(img);")
    client.simple_query("CREATE INDEX ON png.pixels(x);")
    client.simple_query("CREATE INDEX ON png.pixels(y);")
    client.simple_query("CREATE INDEX ON png.pixels(c);")
}

fn encode(file_path: &str, client: &mut Client) {

    let path = Path::new(file_path);
    let file = File::create(path).unwrap();
    let ref mut w = BufWriter::new(file);

    let mut encoder = match make_encoder(client, w) {
        Ok(enc) => enc,
        Err(err) => panic!("Error making encoder: {}.", err)
    };

    match get_palette(client) {
        Ok(p) => encoder.set_palette(p),
        Err(e) => panic!("Error querying for palette: {}.", e)
    };

    let mut writer = encoder.write_header().unwrap();

    match get_pixels(client) {
        Ok(frames) => {
            for pixels in frames {
                writer.write_image_data(&pixels).unwrap();
            }
        },
        Err(e) => panic!("Error querying for pixels: {}.", e)
    };
    writer.finish().unwrap();
}

fn make_encoder<'a>(client: &mut Client, writer: &'a mut BufWriter<File>) -> Result<Encoder<'a, &'a mut BufWriter<File>>, Error> {

    let size_row = client.query_one("SELECT max(img) AS n, max(x) AS w, max(y) AS h FROM png.pixels;", &[])?;
    let n = size_row.get::<&str, i32>("n") as u32;
    let width = size_row.get::<&str, i32>("w") as u32;
    let height = size_row.get::<&str, i32>("h") as u32;

    let mut encoder = png::Encoder::new(writer, width+1, height+1);
    encoder.set_color(png::ColorType::Indexed);
    encoder.set_depth(png::BitDepth::Eight);
    if n > 1 {
        encoder.set_animated(n, 0).unwrap();
        encoder.set_frame_delay(1, 4).unwrap();
    }

    return Ok(encoder);
}

fn get_palette(client: &mut Client) -> Result<Vec<u8>, Error> {

    let mut palette = Vec::new();

    for row in client.query("SELECT r, g, b FROM png.palette ORDER BY i;", &[])? {
        let r = row.get::<&str, i32>("r") as u8;
        let g = row.get::<&str, i32>("g") as u8;
        let b = row.get::<&str, i32>("b") as u8;
        palette.push(r);
        palette.push(g);
        palette.push(b);
    }
    return Ok(palette);
}

fn get_pixels(client: &mut Client) -> Result<Vec<Vec<u8>>, Error> {

    let mut frames = Vec::new();
    let mut pixels = Vec::new();
    let mut cur_img = 1;

    for row in client.query("SELECT img, c FROM png.pixels ORDER BY img, y, x;", &[])? {
        let i = row.get::<&str, i32>("img") as u8;
        if i != cur_img {
            cur_img = i;
            frames.push(pixels.clone());
            pixels = Vec::new();
        }
        let c = row.get::<&str, i32>("c") as u8;
        pixels.push(c);
    }
    frames.push(pixels);
    return Ok(frames);
}

fn exec_script(filename: &str, client: &mut Client) {

    let script = std::fs::read_to_string(filename).unwrap();
    client.batch_execute(&script).unwrap();
}

fn connect(con_str: &str) -> Client {

    match Client::connect(con_str, NoTls) { 
        Ok(c) => c,
        Err(e) => panic!("Error connecting to database: {}.", e)
    }
}
