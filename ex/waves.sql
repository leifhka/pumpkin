INSERT INTO png.palette
SELECT n, 0, n*2, n*2
FROM generate_series(0, 50) AS t(n);

INSERT INTO png.pixels
SELECT f, x, y, mod(f*3 + x + y, 50) AS c
FROM generate_series(1, 30) AS t1(f),
     generate_series(0, 63) AS t2(x),
     generate_series(0, 63) as t3(y);
