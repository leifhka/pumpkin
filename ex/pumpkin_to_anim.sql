INSERT INTO png.palette VALUES
(4, 75, 30, 45), -- lighter purple
(5, 90, 40, 60), -- even lighter
(6, 105, 50, 75); -- even lighter

INSERT INTO png.pixels
SELECT i AS img,
    CASE -- "move" pixel i spaces left
        WHEN x - jump < 0 THEN 255+(x - jump)
        ELSE x - jump
    END AS x,
    y, 
    CASE -- animate glowing eyes
        WHEN c >= 3 AND c <= 6 -- eyes, nose and mouth
            THEN ((c + (i - 3)) % 4) + 3
            ELSE c
        END AS c
FROM png.pixels AS o,
     (SELECT i, i*3 AS jump FROM generate_series(2, 10) AS t(i)) AS t(i, jump);
