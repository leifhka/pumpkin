BEGIN;

INSERT INTO png.palette VALUES
(0, 0, 0, 0),
(1, 0, 100, 100);

INSERT INTO png.pixels
SELECT 1, x, y, round(random()) AS c
FROM generate_series(0, 63) AS t1(x),
     generate_series(0, 63) AS t2(y);

CREATE OR REPLACE FUNCTION next(i int) RETURNS void AS
$body$
INSERT INTO png.pixels
SELECT img, x, y,
  CASE
    WHEN c = 1 AND (live = 3 OR live = 4) THEN 1 -- c is counted as one of the live
    WHEN c = 0 AND live = 3 THEN 1
    ELSE 0
  END AS c
FROM (
    SELECT c.img + 1 AS img, c.x, c.y, c.c, sum(n.c) AS live
    FROM png.pixels AS c JOIN png.pixels AS n
         ON (c.img = n.img AND (abs(c.x - n.x) <= 1 AND abs(c.y - n.y) <= 1))
    WHERE c.img = i
    GROUP BY c.img, c.x, c.y, c.c
  ) t;
$body$ language sql;

SELECT i, next(i)
FROM generate_series(1, 30) AS t(i);

COMMIT;
