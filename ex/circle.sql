INSERT INTO png.palette VALUES
(0, 0, 0, 0),
(1, 0, 200, 100);

INSERT INTO png.pixels
SELECT 1, x, y, 
    (CASE WHEN sqrt(pow(x - 31, 2) + pow(y - 31, 2)) <= 8 THEN 1 ELSE 0 END) AS c
FROM generate_series(0, 63) AS t2(x),
     generate_series(0, 63) as t3(y);
