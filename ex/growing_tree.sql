BEGIN;

INSERT INTO png.palette VALUES
(0, 0, 0, 0), -- black background
(1, 139,69,19), -- brown trunk
(2, 0,100,0); -- green leaves

INSERT INTO png.pixels 
SELECT 1, x, 63, 1 -- bottom trunk (seed)
FROM generate_series(28, 34) AS t(x);

INSERT INTO png.pixels -- black background
SELECT 1, x, y, 0
FROM generate_series(0, 63) AS t1(x),
     generate_series(0, 63) AS t2(y)
WHERE (x, y) NOT IN (SELECT x, y FROM png.pixels);

CREATE OR REPLACE FUNCTION next(i int) RETURNS void AS
$body$
INSERT INTO png.pixels -- growing
SELECT img, x, y,
  CASE
    WHEN c = 0 AND (63-y) BETWEEN (img-1) AND (img+1) AND trunk >= 1 THEN 2
    WHEN c = 2 THEN 1
    ELSE c
  END AS c
FROM (
    SELECT c.img + 1 AS img, c.x, c.y, c.c,
        -- random growth with declining likelyhood (gives branches)
        coalesce(round(pow(0.85, sqrt(c.img))*random()*sum(n.c)), 0) AS trunk
    FROM png.pixels AS c LEFT JOIN png.pixels AS n
         ON (c.img = n.img AND (n.y - c.y = 1 AND abs(c.x - n.x) <= 1))
    WHERE c.img = i
    GROUP BY c.img, c.x, c.y, c.c
  ) t;
$body$ language sql;

SELECT i, next(i)
FROM generate_series(1, 60) AS t(i);

COMMIT;
