INSERT INTO png.palette VALUES
(0, 0, 0, 0),
(1, 0, 100, 100);

INSERT INTO png.pixels
SELECT 1, x, y, 
    (CASE
        WHEN sqrt(pow(x - 31, 2) + pow(y - 31, 2)) <= 5
            THEN 1
        ELSE 0 END
    ) AS c
FROM generate_series(0, 63) AS t2(x),
     generate_series(0, 63) as t3(y);

-- Given some picture above, we can make it move with:
INSERT INTO png.pixels
SELECT f, x, y, c
FROM png.pixels, generate_series(2, 10) AS t(f);

UPDATE png.pixels AS p
SET c = coalesce((SELECT o.c FROM png.pixels AS o WHERE o.img = 1 AND o.y = p.y AND o.x = p.x - p.img), 0);
