INSERT INTO png.palette VALUES
(0, 0, 0, 0), -- black
(1, 240, 125, 0), -- orange
(2, 0, 100, 0), -- green
(3, 60, 20, 30); -- very dark red/blackish

INSERT INTO png.pixels
SELECT 1, x, y,
    CASE
        WHEN 112 <= x AND x <= 144 AND 20 <= y AND y <= 70
            THEN 2 -- green stilk (square)
        WHEN abs(x - 128) + abs(y - 146) <= 12
            THEN 3 -- blackish nose (diamond)
        WHEN abs(x - 84) + abs(y - 120) <= 22
            THEN 3 -- blackish left eye (diamond)
        WHEN abs(x - 172) + abs(y - 120) <= 22
            THEN 3 -- blackish right eye (diamond)
        WHEN abs(x - 107) + abs(y - 180) <= 12 OR
             abs(x - 121) + abs(y - 180) <= 12 OR
             abs(x - 135) + abs(y - 180) <= 12 OR
             abs(x - 149) + abs(y - 180) <= 12
            THEN 3 -- blackish mouth
        WHEN sqrt(pow(x - 128, 2) + 1.5*pow(y - 128, 2)) <= 100
            THEN 1 -- orange elipse
        ELSE 0 -- black background
    END AS c
FROM generate_series(0, 255) AS t1(x),
     generate_series(0, 255) AS t2(y);
