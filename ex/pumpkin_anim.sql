INSERT INTO png.palette VALUES
(0, 0, 0, 0), -- black
(1, 240, 125, 0), -- orange
(2, 0, 100, 0), -- green
(3, 60, 20, 30), -- very dark purple/blackish
(4, 75, 30, 45), -- lighter purple
(5, 90, 40, 60), -- even lighter
(6, 105, 50, 75); -- even lighter

INSERT INTO png.pixels
SELECT i, x, y,
    CASE
        WHEN 112 <= x+jump AND x+jump <= 144 AND 20 <= y AND y <= 70
            THEN 2 -- green stilk (square)
        WHEN abs(x+jump - 128) + abs(y - 146) <= 12
            THEN 3 + mod(i, 4) -- blackish nose (diamond)
        WHEN abs(x+jump - 84) + abs(y - 120) <= 22
            THEN 3 + mod(i, 4) -- blackish left eye (diamond)
        WHEN abs(x+jump - 172) + abs(y - 120) <= 22
            THEN 3 + mod(i, 4) -- blackish right eye (diamond)
        WHEN abs(x+jump - 107) + abs(y - 180) <= 12 OR
             abs(x+jump - 121) + abs(y - 180) <= 12 OR
             abs(x+jump - 135) + abs(y - 180) <= 12 OR
             abs(x+jump - 149) + abs(y - 180) <= 12
            THEN 3 + mod(i, 4) -- blackish mouth
        WHEN sqrt(pow(x+jump - 128, 2) + 1.5*pow(y - 128, 2)) <= 100
            THEN 1 -- orange elipse
        ELSE 0 -- black background
    END AS c
FROM generate_series(0, 255) AS t1(x),
     generate_series(0, 255) AS t2(y),
     (SELECT i, i*3 FROM generate_series(1, 16) AS t(i)) AS t3(i, jump);

