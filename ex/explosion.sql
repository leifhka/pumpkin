INSERT INTO png.palette
SELECT n, n*20, n*10, n*5
FROM generate_series(0, 10) AS t(n);

INSERT INTO png.pixels
SELECT f, x, y, 
    (CASE
        WHEN sqrt(pow(x - 31, 2) + pow(y - 31, 2)) <= f
            THEN sqrt(pow(x - 31, 2) + pow(y - 31, 2))/3
        ELSE 0 END
    ) AS c
FROM generate_series(1, 30) AS t1(f),
     generate_series(0, 63) AS t2(x),
     generate_series(0, 63) as t3(y);
