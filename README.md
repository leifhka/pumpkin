![Pumpkin](pumpkin.png)

# pumpkin

A simple CLI-program for translating indexed (A)PNG images
to PostgreSQL tables (according to the below schema)
and back, written in Rust.

The schema translated from/to is as follows:

```sql
CREATE TABLE png.palette(i int, r int, g int, b int);
CREATE TABLE png.pixels(img int, x int, y int, c int);
```

where `i` is the color index, `(r, g, b)` denotes the color value
in `png.palette`, and
`img` is the image counter, `x` is the x-coordinate of the pixel,
`y` is the y-coordinat of the pixel, and `c` is an int referencing
a color in `png.palette`.

So `png.palette` contains the PNG's palette and
`png.pixels` contains the actual image's pixels.
This simple structure makes it very easy to use SQL
to either transform already existing images,
or generate completely new images.

The program also supports APNG, i.e. animated PNGs
with multiple images. Each image is simply
denoted by the `img`-column, starting at `1`,
and is ordered sequentially.

## Example

The above image can be generated with:

```bash
pumpkin "user=<user> dbname=<dbname> host=<host> password=<password>" --script ex/pumpkin.sql --encode pumpkin.png
```

where `ex/pumpkin.sql` contains:

```sql
INSERT INTO png.palette VALUES
(0, 0, 0, 0), -- black
(1, 240, 125, 0), -- orange
(2, 0, 100, 0), -- green
(3, 60, 20, 30); -- very dark red/blackish

INSERT INTO png.pixels
SELECT 1, x, y,
    CASE
        WHEN 112 <= x AND x <= 144 AND 20 <= y AND y <= 70
            THEN 2 -- green stilk (square)
        WHEN abs(x - 128) + abs(y - 146) <= 12
            THEN 3 -- blackish nose (diamond)
        WHEN abs(x - 84) + abs(y - 120) <= 22
            THEN 3 -- blackish left eye (diamond)
        WHEN abs(x - 172) + abs(y - 120) <= 22
            THEN 3 -- blackish right eye (diamond)
        WHEN abs(x - 107) + abs(y - 180) <= 12 OR
             abs(x - 121) + abs(y - 180) <= 12 OR
             abs(x - 135) + abs(y - 180) <= 12 OR
             abs(x - 149) + abs(y - 180) <= 12
            THEN 3 -- blackish mouth
        WHEN sqrt(pow(x - 128, 2) + 1.5*pow(y - 128, 2)) <= 100
            THEN 1 -- orange elipse
        ELSE 0 -- black background
    END AS c
FROM generate_series(0, 255) AS t1(x),
     generate_series(0, 255) AS t2(y);
 
```

If we change the `ex/pumpkin.sql`-code slightly (stored as `ex/pumpkin_anim.sql`) to:

```sql
INSERT INTO png.palette VALUES
(0, 0, 0, 0), -- black
(1, 240, 125, 0), -- orange
(2, 0, 100, 0), -- green
(3, 60, 20, 30), -- very dark purple/blackish
(4, 75, 30, 45), -- lighter purple
(5, 90, 40, 60), -- even lighter
(6, 105, 50, 75); -- even lighter

INSERT INTO png.pixels
SELECT i, x, y,
    CASE
        WHEN 112 <= x+jump AND x+jump <= 144 AND 20 <= y AND y <= 70
            THEN 2 -- green stilk (square)
        WHEN abs(x+jump - 128) + abs(y - 146) <= 12
            THEN 3 + mod(i, 4) -- blackish nose (diamond)
        WHEN abs(x+jump - 84) + abs(y - 120) <= 22
            THEN 3 + mod(i, 4) -- blackish left eye (diamond)
        WHEN abs(x+jump - 172) + abs(y - 120) <= 22
            THEN 3 + mod(i, 4) -- blackish right eye (diamond)
        WHEN abs(x+jump - 107) + abs(y - 180) <= 12 OR
             abs(x+jump - 121) + abs(y - 180) <= 12 OR
             abs(x+jump - 135) + abs(y - 180) <= 12 OR
             abs(x+jump - 149) + abs(y - 180) <= 12
            THEN 3 + mod(i, 4) -- blackish mouth
        WHEN sqrt(pow(x+jump - 128, 2) + 1.5*pow(y - 128, 2)) <= 100
            THEN 1 -- orange elipse
        ELSE 0 -- black background
    END AS c
FROM generate_series(0, 255) AS t1(x),
     generate_series(0, 255) AS t2(y),
     (SELECT i, i*3 FROM generate_series(1, 16) AS t(i)) AS t3(i, jump);
```

we get the following animation with the corresponding command as above:

![Animated pumpkin](pumpkin_anim.png)

The same animation could also be generated based on using `pumpkin.png`
(i.e. the output of the first command) as input, and the use the following
script (from `ex/pumpkin_to_anim.sql`) to add animated images based on the first:

```sql
INSERT INTO png.palette VALUES
(4, 75, 30, 45), -- lighter purple
(5, 90, 40, 60), -- even lighter
(6, 105, 50, 75); -- even lighter

INSERT INTO png.pixels
SELECT i AS img,
    CASE -- "move" pixel i spaces left
        WHEN x - jump < 0 THEN 255+(x - jump)
        ELSE x - jump
    END AS x,
    y, 
    CASE -- animate glowing eyes
        WHEN c >= 3 AND c <= 6 -- eyes, nose and mouth
            THEN ((c + (i - 3)) % 4) + 3
            ELSE c
        END AS c
FROM png.pixels AS o,
     (SELECT i, i*3 AS jump FROM generate_series(2, 10) AS t(i)) AS t(i, jump);
```

with the command:

```bash
pumpkin "user=<user> dbname=<dbname> host=<host> password=<password>" --init --decode pumpkin.png --script ex/pumpkin_to_anim.sql --encode pumpkin_anim.png
```

See more examples in the `ex`-folder, and more examples of commandline use below.

## Build

Simply execute

```bash
cargo build --release
```

and the program is available under `target/release/pumpkin`.

## Use

To decode (i.e. translate a (A)PNG to tables) simply run

```bash
pumpkin "user=<user> dbname=<dbname> host=<host> password=<password>" --decode <image>
```

where `<user>`, `<dbname>`, `<host>` and `<password>` are the username, database name,
host, and password for the PostgreSQL database to use, and `<image>` is the
path to the (A)PNG to decode.

To encode (i.e. translate tables to (A)PNG) simply run

```bash
pumpkin "user=<user> dbname=<dbname> host=<host> password=<password>" --encode <image>
```

where `<user>`, `<dbname>`, `<host>` and `<password>` are as above, and `<image>` is the
path to the (A)PNG to write to.

To simply create an empty schema (i.e. an empty `png.palette` and `png.pixels` tables)
(e.g. to use SQL to generate an image), simply run

```bash
pumpkin "user=<user> dbname=<dbname> host=<host> password=<password>" --init
```

One can also execute an SQL-script with:

```bash
pumpkin "user=<user> dbname=<dbname> host=<host> password=<password>" --script <script>
```

where `<script>` is an SQL-script. All of these commands can be combined, so one
can e.g. both decode a file `<dfile>`, execute
an SQL-script, `<script>`, over it, and then encode the result as `<efile>` with:

```bash
pumpkin "user=<user> dbname=<dbname> host=<host> password=<password>" --decode <dfile> --script <script> --encode <efile>
```
